

$(function () {  //该语法为：文档就绪事件  http://www.runoob.com/jquery/jquery-syntax.html

    // 开始写 jQuery 代码...
    //点击事件
    $("#test").click(function () {

        var data = { phone: "13800138000" };
        // 语法:
        // $.post(URL,data,callback);
        //使用ajax方法  http://www.runoob.com/jquery/jquery-ajax-get-post.html
        $.ajax({
            type: "POST",  //请求类型，常用有Get,Post
            url: "http://120.79.65.57:8080/api/services/app/Szkyjf/SendSms", //请求的url
            contentType: "application/json; charset=utf-8", //内容类型，表示json,utf-8字符集
            data: JSON.stringify(data),//请求数据
            dataType: "json",//请求的数据类型
            success: function (result) {  //请求成功执行的函数
                console.log(result);
                if(result.success){
                    alert("发送验证码成功");
                }else{
                    alert("发送验证码失败");
                }
            },
            error: function (message) { //请求失败执行的函数
                console.log("提交数据失败！");
            }
        });

    });

});